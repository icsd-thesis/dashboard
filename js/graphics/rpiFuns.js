/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */
/* global raspber_controler_automation_port, raspber_controler_automation_ip */

var kitchen_exaust_switch_act = document.getElementById("kitchen_exaust_switch_act");
var living_room_exaust_switch_act = document.getElementById("living_room_exaust_switch_act");
var bedroom_exaust_switch_act = document.getElementById("bedroom_exaust_switch_act");
var kitchen_radiator_switch_act = document.getElementById("kitchen_radiator_switch_act");
var living_room_radiator_switch_act = document.getElementById("living_room_radiator_switch_act");
var bedroom_radiator_switch_act = document.getElementById("bedroom_radiator_switch_act");

var bedroom_lights_switch_act = document.getElementById("bedroom_lights_switch_act");
var living_room_lights_switch_act = document.getElementById("living_room_lights_switch_act");
var kitchen_lights_switch_act = document.getElementById("kitchen_lights_switch_act");


function loadGridLayout(json) {
//    load_second_row("second_row_1","second_row_2");
    $("#rpi_loader").fadeOut("slow", function () {
        load_up_first_row("first_row");
        setSwitchFromData(json);
    });
    load_second_column("kitchen", "living_room", "bedroom");


    //load 3 colum

    load_up_fits_column("kitchen_radiator", "living_room_radiator", "bedroom_radiator");

    $("#kitchen_radiator_switch").fadeIn(100);
    $("#living_room_radiator_switch").fadeIn(100);
    $("#bedroom_radiator_switch").fadeIn(100);
    $("#kitchen_exaust_switch").fadeIn(100);
    $("#living_room_exaust_switch").fadeIn(100);
    $("#bedroom_exaust_switch").fadeIn(100);
    $("#bedroom_lights_switch").fadeIn(100);
    $("#living_room_lights_switch").fadeIn(100);
    $("#kitchen_lights_switch").fadeIn(100);

    kitchen_radiator_switch_act = document.getElementById("kitchen_radiator_switch_act");
    living_room_radiator_switch_act = document.getElementById("living_room_radiator_switch_act");
    bedroom_radiator_switch_act = document.getElementById("bedroom_radiator_switch_act");
    kitchen_exaust_switch_act = document.getElementById("kitchen_exaust_switch_act");
    living_room_exaust_switch_act = document.getElementById("living_room_exaust_switch_act");
    bedroom_exaust_switch_act = document.getElementById("bedroom_exaust_switch_act");

    bedroom_lights_switch_act = document.getElementById("bedroom_lights_switch_act");
    living_room_lights_switch_act = document.getElementById("living_room_lights_switch_act");
    kitchen_lights_switch_act = document.getElementById("kitchen_lights_switch_act");
    //load 4 colum

    kitchen_exaust_switch_act.addEventListener('click', function () {
        if ($('#kitchen_exaust_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "exaust", "kitchen", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "exaust", "kitchen", "stop");

        }
    });
    living_room_exaust_switch_act.addEventListener('click', function () {
        if ($('#living_room_exaust_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "exaust", "living_room", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "exaust", "living_room", "stop");

        }
    });
    bedroom_exaust_switch_act.addEventListener('click', function () {
        if ($('#bedroom_exaust_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "exaust", "bedroom", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "exaust", "bedroom", "stop");

        }
    });

    kitchen_radiator_switch_act.addEventListener('click', function () {
        if ($('#kitchen_radiator_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "radiator", "kitchen", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "radiator", "kitchen", "stop");

        }
    });
    living_room_radiator_switch_act.addEventListener('click', function () {
        if ($('#living_room_radiator_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "radiator", "living_room", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "radiator", "living_room", "stop");

        }
    });
    bedroom_radiator_switch_act.addEventListener('click', function () {
        if ($('#bedroom_radiator_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "radiator", "bedroom", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "radiator", "bedroom", "stop");

        }
    });

    bedroom_lights_switch_act.addEventListener('click', function () {
        if ($('#bedroom_lights_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "Lights", "bedroom", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "Lights", "bedroom", "stop");

        }
    });
    living_room_lights_switch_act.addEventListener('click', function () {
        if ($('#living_room_lights_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "Lights", "living_room", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "Lights", "living_room", "stop");

        }
    });
    kitchen_lights_switch_act.addEventListener('click', function () {
        if ($('#kitchen_lights_switch_act').is(":checked"))
        {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "Lights", "kitchen", "start");
        } else {
            getRpiRequest(raspber_controler_automation_ip, raspber_controler_automation_port, "trigger", setSwitchFromData, "Lights", "kitchen", "stop");

        }
    });

}
function setSwitchFromData(json) {
    console.log("ISNISFADSADSADS");
    let jsonData = JSON.parse(json);
    console.log(jsonData);
    for (var i = 0; i < jsonData.length; i++) {
        console.log(jsonData[i].name);
        switch (jsonData[i].name) {
            case "LIVING_ROOM_Radiator":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("living_room_radiator_switch_act").checked = true;
                break;
            case "BEDROOM_Radiator":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("bedroom_radiator_switch_act").checked = true;
                break;
            case "KITCHEN_Radiator":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("kitchen_radiator_switch_act").checked = true;
                break;
            case "LIVING_ROOM_Exaust":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("living_room_exaust_switch_act").checked = true;
                break;
            case "BEDROOM_Exaust":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("bedroom_exaust_switch_act").checked = true;
                break;
            case "KITCHEN_Exaust":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("kitchen_exaust_switch_act").checked = true;
                break;
            case "KITCHEN_Lights":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("kitchen_lights_switch_act").checked = true;
                break;
            case "BEDROOM_Lights":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("bedroom_lights_switch_act").checked = true;
                break;
            case "LIVING_ROOM_Lights":
                if (jsonData[i].state === "HIGH")
                    document.getElementById("living_room_lights_switch_act").checked = true;
                break;

        }
    }

}
function onErrorPrint() {
    $("#rpi_loader").fadeOut("slow", function () {
        load_down_first_row("first_row");
    });
    $("#rpi_loader1").fadeOut("slow", function () {
        load_down_third_row("kitchen_exaust", "living_room_exaust", "bedroom_exaust");
    });
    $("#rpi_loader2").fadeOut("slow", function () {
        load_down_third_row("kitchen_radiator", "living_room_radiator", "bedroom_radiator");
    });
}
function load_up_first_row(container) {
    $('#' + container).append(appendGridElemment("rpi_up.jpg"));
}
function load_up_fits_column(container1, container2, container3, container4, container5, container6) {
    $('#' + container1).append(appendGridElemment("kitchen_up.png"));
    $('#' + container2).append(appendGridElemment("living_room_up.png"));
    $('#' + container3).append(appendGridElemment("bedroom_up.png"));
}
function load_down_first_row(container) {
    $('#' + container).append(appendGridElemment("rpi_down.jpg"));
}
function load_down_third_row(container1, container2, container3, container4, container5, container6) {
    $('#' + container1).append(appendGridElemment("kitchen_down.png"));
    $('#' + container2).append(appendGridElemment("living_room_down.png"));
    $('#' + container3).append(appendGridElemment("bedroom_down.png"));
}

function load_second_column(container1, container2, container3) {
    $("#kitchen_loader").fadeOut("slow", function () {
        $('#' + container1).append(appendGridElemment("kitchen_up.png"));
    });
    $("#living_loader").fadeOut("slow", function () {
        $('#' + container2).append(appendGridElemment("living_room_up.png"));
    });
    $("#bedroom_loader").fadeOut("slow", function () {
        $('#' + container3).append(appendGridElemment("bedroom_up.png"));
    });
}


function appendGridElemment(image) {
    return ' <img  width="100" height="100" style="max-height: \n\
        50%; max-width: 50%; display: block;margin-left: auto; \n\
        margin-right: auto;" src="./images/services_icons/' + image + '"\n\
        alt="Be patient..." />'
}


